package prime.catalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import prime.dao.CatalogDAO;

import java.util.Date;
import java.util.List;

@Component
public class Catalog {

    @Autowired
    private CatalogDAO catalogDAO;

    public void setCatalogDAO(CatalogDAO catalogDAO) {
        this.catalogDAO = catalogDAO;
    }

    public List<Book> getAll(){
        return catalogDAO.getAll();
    }

    public void add(Book book){
        if (book != null) catalogDAO.add(book);
    }

    public void remove(Book book){
        if (book != null) catalogDAO.remove(book);
    }

    public void edit(Book editedBook, Book result){
        catalogDAO.edit(editedBook, result);
    }

    public List<Book> filterByName(String abc){
        if (abc == null) return null;
        return catalogDAO.filterByName(abc);
    }

    public List<String> getAllAuthors(){
        return catalogDAO.getAllAuthors();
    }

    public List<Book> getByAuthor(String author){
        if (author == null) return null;
        return catalogDAO.getByAuthor(author);
    }

    public List<String> getAllYears(){
        return catalogDAO.getAllYears();
    }

    public List<Book> getByYear(Date year){
        return catalogDAO.getByYear(year);
    }

    public void showAll(){
        for (Book book : getAll()) {
            System.out.println(book.toString());
        }
    }
}
