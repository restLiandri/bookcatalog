package prime.dao;

import org.springframework.jdbc.core.RowMapper;
import prime.catalog.Book;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookRowMapper implements RowMapper<Book> {
    @Override
    public Book mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Book(resultSet.getString("title"),
                resultSet.getString("author"),
                resultSet.getDate("publicationYear").toLocalDate());
    }
}
