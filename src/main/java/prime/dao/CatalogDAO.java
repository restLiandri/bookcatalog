package prime.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import prime.catalog.Book;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CatalogDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @PostConstruct
    public void init() {
        try {
            jdbcTemplate.update("CREATE TABLE books (\n" +
                    "  title VARCHAR(50),\n" +
                    "  author VARCHAR(50),\n" +
                    "  publicationYear DATE)");
            add(new Book("Этюд в багровых тонах", "Дойл Артур Конан", LocalDate.of(1887, 1, 1)));
            add(new Book("Бесы", "Достоевский Фёдор", LocalDate.of(1872, 1, 1)));
            add(new Book("Братья Карамазовы", "Достоевский Фёдор", LocalDate.of(1880, 1, 1)));
            add(new Book("Они сражались за Родину", "Шолохов Михаил", LocalDate.of(1943, 1, 1)));
            add(new Book("Тарас Бульба", "Гоголь Николай", LocalDate.of(1835, 1, 1)));
        }
        catch (DataAccessException e) {
        }
    }

    private static List<Book> catalog = new ArrayList<>();

    public List<Book> getAll() {
        return jdbcTemplate.query("SELECT * FROM books", new BookRowMapper());
    }

    public void add(Book book) {
        jdbcTemplate.update("INSERT INTO books " +
                "VALUES (?, ?, ?)", book.getTitle(), book.getAuthor(),
                new Date(book.getPublicationYear().getYear() - 1900, 0, 1));
    }

    public void remove(Book book) {
        jdbcTemplate.update("DELETE FROM books " +
                        "WHERE title = ? AND author = ? AND publicationYear = ?", book.getTitle(), book.getAuthor(),
                new Date(book.getPublicationYear().getYear() - 1900, 0, 1));
    }

    public void edit(Book editedBook, Book result) {
        catalog.set(catalog.indexOf(editedBook), result);
    }

    public List<Book> filterByName(String abc) {
        return jdbcTemplate.query("SELECT * FROM books WHERE title LIKE ? ", new Object[] {"" + abc + "%"}, new BookRowMapper());
    }

    public List<String> getAllAuthors() {
        return jdbcTemplate.queryForList("SELECT DISTINCT author FROM books", String.class);
    }

    public List<Book> getByAuthor(String author) {
        return jdbcTemplate.query("SELECT * FROM books WHERE author = ?", new Object[] {author}, new BookRowMapper());
    }

    public List<String> getAllYears() {
        List<Date> tempResult = jdbcTemplate.queryForList("SELECT DISTINCT publicationYear FROM books", Date.class);
        List<String> result = new ArrayList<>();
        for (Date date : tempResult) {
            result.add(new Integer(date.getYear() + 1900).toString());
        }
        return result;
    }

    public List<Book> getByYear(Date publicationYear) {
        return jdbcTemplate.query("SELECT * FROM books WHERE publicationYear = ?",
                new Object[] {publicationYear}, new BookRowMapper());
    }
}
