package prime.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import prime.catalog.Book;
import prime.catalog.Catalog;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Path("/books")
public class CatalogService {

    private ApplicationContext ctx = new AnnotationConfigApplicationContext(prime.spring.AppConfig.class);
    private Catalog catalog = (Catalog) ctx.getBean("catalog");

    @GET
    @Path("/getAll")
    @Produces("application/json")
    public Response getAll() {

        List<Book> allBooks = catalog.getAll();

        return Response.status(200).entity(allBooks).build();
    }

    @GET
    @Path("/addBook/title={title}&author={author}&year={year : \\d\\d\\d\\d}")
    public Response add(@PathParam("title") String title,
                        @PathParam("author") String author,
                        @PathParam("year") String yearStr) {
        Book book = new Book(title, author, LocalDate.of(Integer.parseInt(yearStr), 1, 1));
        catalog.add(book);
        return Response.status(201).entity(book).build();
    }

    @GET
    @Path("/removeBook/title={title}&author={author}&year={year : \\d\\d\\d\\d}")
    public Response remove(@PathParam("title") String title,
                        @PathParam("author") String author,
                        @PathParam("year") String yearStr) {
        Book book = new Book(title, author, LocalDate.of(Integer.parseInt(yearStr), 1, 1));
        catalog.remove(book);
        return Response.status(201).entity(book).build();
    }

    @GET
    @Path("/filterByName={abc}")
    @Produces("application/json")
    public Response filterByName(@PathParam("abc") String abc) {

        List<Book> result = catalog.filterByName(abc);

        return Response.status(200).entity(result).build();
    }

    @GET
    @Path("/getAllAuthors")
    @Produces("application/json")
    public Response getAllAuthors() {

        List<String> result = catalog.getAllAuthors();

        return Response.status(200).entity(result).build();
    }

    @GET
    @Path("/getByAuthor={author}")
    @Produces("application/json")
    public Response getByAuthor(@PathParam("author") String author) {

        List<Book> result = catalog.getByAuthor(author);

        return Response.status(200).entity(result).build();
    }

    @GET
    @Path("/getAllYears")
    @Produces("application/json")
    public Response getAllYears() {

        List<String> result = catalog.getAllYears();

        return Response.status(200).entity(result).build();
    }

    @GET
    @Path("/getByYear={year : \\d\\d\\d\\d}")
    @Produces("application/json")
    public Response getByYear(@PathParam("year") String yearStr) {
        int year = Integer.parseInt(yearStr);

        List<Book> result = catalog.getByYear(new Date(year - 1900, 0, 1));

        return Response.status(200).entity(result).build();
    }

}
